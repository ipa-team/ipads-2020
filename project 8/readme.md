# Readme

Due to the overall complexity of the project and the immense effort we put into it, we were not able to stick to the given word limit of 1500 words.

## Colab Notebook:
Final skript on google colab
https://colab.research.google.com/drive/1kv-DB6rxPH2z_DRmmT4VdBFIfKnXisWH?usp=sharing
The notebook is also available in git as: "Project 8.ipynb"
The notebook Project8.3.ipynb contains the solution of task 8.3

Additional datasets that were generated during the process are stored in the data folder.
Preparation scripts for this are stored in data preparation.
To run the Colab notebook and access the database in Google BigQuery, one needs to be part of the project team of the Google BigQuery Project. If needed, we can add you to this project, so that you are able to run the queries.

## Contributions
Commits do not represent contribution since work was also done and contributed outside this repository.

## Link to Overleaf Report
https://www.overleaf.com/4349392865qpvdmfmxhqzw
The report is available in git as: "Report 8.pdf"