import json
import pandas as pd

from pathlib import Path
from typing import List

folder_path = r'C:\Users\deneb\Downloads\student_life'
stress_folder_path = Path(folder_path) / 'EMA' / 'Stress'
activity_folder_path = Path(folder_path) / 'sensing' / 'activity'
gps_folder_path = Path(folder_path) / 'sensing' / 'gps'

good_ids = ['u00', 'u01', 'u02', 'u03', 'u04', 'u05', 'u07', 'u09', 'u10',
             'u14', 'u15', 'u16', 'u17', 'u18', 'u19', 'u20', 'u23', 'u24',
             'u27', 'u30', 'u31', 'u32', 'u33', 'u34', 'u35', 'u36', 'u42',
             'u43', 'u44', 'u45', 'u47', 'u49', 'u51', 'u52', 'u53', 'u54',
             'u56', 'u58', 'u59', 'u46']


def files_in(folder):
    return [fs_entity for fs_entity in Path(folder).rglob('*.*') if fs_entity.is_file()]


def contains_good_id(file: Path, ids: List[str]):
    for id in ids:
        if id in file.name:
            return True
    return False


c = 1


def try_remove(file: Path, ids: List[str]):
    global c
    if not contains_good_id(file, ids):
        print(f'{c} Removing: {file.absolute()}')
        file.unlink()
        c += 1


def remove_trash(subfolder: str = ''):
    trash_path = folder_path if subfolder == '' else subfolder
    for f in files_in(trash_path):
        try_remove(f, good_ids)


def merge_csv(files):
    result = pd.DataFrame()
    for file in files:
        frame = pd.read_csv(file, index_col=False)
        frame['id'] = file.stem[-3:]
        result = result.append(frame)
    return result


def merge_csv_in(path: Path, result_name: str):
    merge_csv(files_in(path)).to_csv(path / result_name, index=False)


def filer_null(json_array):
    return [obj for obj in json_array if not 'null' in obj and len(obj) > 1]


def json_to_csv_row(json_obj):
    location = json_obj["location"]
    location = location.replace(',', ' ')
    return f'{json_obj["level"]},{location},{json_obj["resp_time"]},{json_obj["id"]}\n'


def csv_headers():
    return 'level,location,resp_time,id\n'


flatter = lambda t: [item for sublist in t for item in sublist]


def add_ids(json_array, file: Path):
    for j in json_array:
        j['id'] = file.stem[-3:]
    return json_array


def merge_json_files(folder: Path):
    data = []
    for f in files_in(str(folder.absolute())):
        data.append(add_ids(filer_null(json.loads(open(f).read())), f))
    return flatter(data)


def json_to_csv(json_array, csv_file: str):
    with open(csv_file, 'w') as w:
        w.write(csv_headers())
        for j in json_array:
            w.write(json_to_csv_row(j))


remove_trash(gps_folder_path)
# json_to_csv(merge_json_files(stress_folder_path), stress_folder_path / 'stress.csv')
# merge_csv_in(activity_folder_path, 'activity.csv')
merge_csv_in(gps_folder_path, 'gps.csv')