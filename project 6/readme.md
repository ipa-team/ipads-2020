# Readme

The final skript is called Project 6.ipynb
The final report is called Report 6.pdf

## Colab Notebook:
The local notebook is a copy of the google colab notebook: 
https://colab.research.google.com/drive/1S8VPcv3ZokcuUZ2ujeCT7xMDdFrDO_nc?usp=sharing

## Contributions
Commits do not represent contribution since work was also done and contributed outside this repository.

## Link to Overleaf Report
https://www.overleaf.com/6287624749kqkzvjktwyqs