# Readme

The final skript is called Project 9.ipynb
The final report is called Report 9.pdf

## Colab Notebook:
The local notebook is a copy of the google colab notebook: 
https://colab.research.google.com/drive/1aSHEXc2l2re6yC2WHKp_6oe2rgw9Mxsa?usp=sharing

## Contributions
Commits do not represent contribution since work was also done and contributed outside this repository.

## Link to Overleaf Report
https://www.overleaf.com/3837661282vjdtrjrjxvcx