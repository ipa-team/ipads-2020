# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 23:11:07 2020

@author: rapha
"""
import numpy as np
import pandas as pd
import os.path
import sklearn

import matplotlib.pyplot as plt

def accuracy(y_true, y_pred):
    
    """ 
    Function to calculate the accuracy of a binary classifier 
    
    Parameters:
    ---
    y_true: numpy array,
            ground truth values to be predicted
        
    y_pred: numpy array, 
            values predicted from model
    
    Returns:
    ---
    The accuracy of the binary model
    """
    
    #True Positives
    tp = sum(np.logical_and(y_true == True, y_pred == True))
    
    #True Negatives
    tn = sum(np.logical_and(y_true == False, y_pred == False))
    
    #False Positives
    fp = sum(np.logical_and(y_true == False, y_pred == True))
    
    #False Negatives
    fn = sum(np.logical_and(y_true == True, y_pred == False))
    
    acc = (tp + tn)/(tp + tn + fp + fn)
    return acc


def evaluation(y_true, y_pred):
    
    """ 
    Function to calculate the accuracy of a binary classifier 
    
    Parameters:
    ---
    y_true: numpy array,
            ground truth values to be predicted
        
    y_pred: numpy array, 
            values predicted from model
    
    Returns:
    ---
    A tuple with the following values:
    (sensitivity, specificity, accuracy, precision, recall)
    """
    #True Positives
    tp = sum(np.logical_and(y_true == True, y_pred == True))
    
    #True Negatives
    tn = sum(np.logical_and(y_true == False, y_pred == False))
    
    #False Positives
    fp = sum(np.logical_and(y_true == False, y_pred == True))
    
    #False Negatives
    fn = sum(np.logical_and(y_true == True, y_pred == False))
    
    acc = (tp + tn)/(tp + tn + fp + fn)
        
    sens = tp/(fn+tp)
    
    spec = tn/(fp+tn)
    
    prec = tp/(tp+fp)
    
    rec = tp/(tp + fn)
    
    return ( sens, spec, acc, prec, rec)

def plot_ROC_curve(y_true, y_probs, save_path_title = None):
    """
    Parameters
    ----------
    y_true : list/array of bool/int
        Classification ground truth values.
    y_probs : list/array of double, 
        Probability for classification as true.
    save_path_title : string, optional
        Filename, under which the ROC curve should be saved. The default is None.

    Returns
    -------
    auc_value : double
        Area under Curve value.

    """
    fpr, tpr, thresholds = sklearn.metrics.roc_curve(y_true, y_probs) 
    auc_value = sklearn.metrics.roc_auc_score(y_true, y_probs)
    plt.plot(fpr[1:], tpr[1:], label = "ROC curve")
    plt.plot(list(range(0,2)), list(range(0,2)), label = "Random")
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title("ROC-Curve, AUC = {}".format(auc_value))

    if save_path_title != None:
        plt.savefig(save_path_title)
    return auc_value
    

def dataFrameFromURLOrFile(url, filepath, columnheader=None):
    if os.path.isfile(filepath) == False:
        data = np.genfromtxt(url, delimiter = ",")
        file_df = pd.DataFrame(data=data, columns=columnheader)
        file_df.to_csv(filepath)
    else:
        file_df = pd.read_csv(filepath, names=columnheader, header=0, index_col=False)
    return file_df
