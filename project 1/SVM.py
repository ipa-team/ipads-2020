# -*- coding: utf-8 -*-
"""
@author: rapha
"""

import os
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.preprocessing import OneHotEncoder


#from utils import accuracy, evaluation


def evaluation(y_true, y_pred):
    
    """ 
    Function to calculate the accuracy of a binary classifier 
    
    Parameters:
    ---
    y_true: numpy array,
            ground truth values to be predicted
        
    y_pred: numpy array, 
            values predicted from model
    
    Returns:
    ---
    A tuple with the following values:
    (percent correctly classified,  sensitivity, specificity, accuracy)
    """
    #True Positives
    tp = sum(np.logical_and(y_true == True, y_pred == True))
    
    #True Negatives
    tn = sum(np.logical_and(y_true == False, y_pred == False))
    
    #False Positives
    fp = sum(np.logical_and(y_true == False, y_pred == True))
    
    #False Negatives
    fn = sum(np.logical_and(y_true == True, y_pred == False))
    
    acc = (tp + tn)/(tp + tn + fp + fn)
    
    pc_correct = sum(y_true == y_pred)/len(y_true)
    
    sens = tp/(fn+tp)
    
    spec = tn/(fp+tn)
    
    return (pc_correct, sens, spec, acc)



# load data from original source
cleveland= np.genfromtxt("https://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.cleveland.data",
                         delimiter = ",")

## data cleansing and preparation for training on cleveland dataset
# find missing values in cleveland dataset
missing_values_c = np.argwhere(np.isnan(cleveland))

# missing values in six instances, removing instances from dataset
missing_rows_c = missing_values_c[:,0]
cleveland_rem = np.delete(cleveland, obj = missing_rows_c, axis = 0)

#check if there are still missing values in the dataset
(np.argwhere(np.isnan(cleveland_rem))).size == 0

# encode categorical values in data:
# categorical data columns, zero based: 2, 6, 10, 12
cat = [2,6,10,12]
enc = OneHotEncoder(sparse=False)
cleveland_encoded = enc.fit_transform(cleveland_rem[:,cat])
cleveland_enc_rem = np.append(cleveland_encoded, np.delete(cleveland_rem, cat, 1), axis = 1)

# Split data into features (x) and target (y)
clev_x = cleveland_enc_rem[:,0:22]
clev_y = cleveland_enc_rem[:,22]

# make target data binary
# presence of CHD: 1 (orig: 1,2,3,4), absence: 0
clev_y = clev_y > 0


# evaluate model
correct, sens, spec, acc =[],[],[],[]

for i in range(100):
    ## split dataset into test and training data
    x_train, x_test, y_train, y_test = train_test_split(clev_x, clev_y, test_size = .25)
    ## Train a Support vector machine for cleveland training and cleveland evaluation data
    # Defining the classifier
    classifier = SVC(kernel="linear", probability = True)
    
    # Training the classifier
    classifier.fit(x_train, y_train)
    
    # Testing the classifier
    y_pred = classifier.predict(x_test)
    y_probs = classifier.predict_proba(x_test)[:,1]

    test_correct, test_sens, test_spec, test_acc = evaluation(y_test, y_pred)
    correct.append(test_correct)
    sens.append(test_sens)
    spec.append(test_spec)
    acc.append(test_acc)
    
    
print("The percentage of correctly predicted targets in cleveland dataset is {}".format(np.mean(test_correct)))
print("The test sensitivity is {}".format(np.mean(test_sens)))
print("The test specificity is {}".format(np.mean(test_spec)))
print("The test accuracy is {}".format(np.mean(test_acc)))
print("\n")


# validating SVM machine trained on all cleveland data with hungarian dataset
hungarian = np.genfromtxt("https://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.hungarian.data",
                         delimiter = ",")

## data cleansing and preparation for training on hungarian dataset
# rows 10 to 12 missing for every entry in hungarian dataset
# training a classifier without variables 10 to 12
# removing them from hungarian and cleveland dataset for training
cleveland_alltraining = cleveland_rem[:,np.r_[0:10,13]]
hungarian_alltesting = hungarian[:,np.r_[0:10,13]]

# find missing values in hungarian dataset
missing_values_h = np.argwhere(np.isnan(hungarian_alltesting))

# remove rows with further missing values from hungarian dataset
missing_rows_h = missing_values_h[:,0]
hungarian_atest_rem = np.delete(hungarian_alltesting, obj = missing_rows_h, axis = 0)


# encode categorical values in data:
# categorical data columns, zero based: 2, 6, 10, 12
cat1 = [2,6]
enc = OneHotEncoder(sparse=False)
cleveland_atrain_encoded = enc.fit_transform(cleveland_alltraining[:,cat1])
cleveland_atrain_enc = np.append(cleveland_atrain_encoded, np.delete(cleveland_alltraining, cat1, 1), axis = 1)

hungarian_atest_encoded = enc.fit_transform(hungarian_atest_rem[:,cat1])
hungarian_atest_enc = np.append(hungarian_atest_encoded, np.delete(hungarian_atest_rem, cat1, 1), axis = 1)



# split data into features (x) and target (y) for train (cleveland) and test (hungarian) dataset
clev_atrain_x = cleveland_atrain_enc[:,0:15]
clev_atrain_y = cleveland_atrain_enc[:,15]

hungarian_atest_x = hungarian_atest_enc[:,0:15]
hungarian_atest_y = hungarian_atest_enc[:,15]

# make target data binary
# presence of CHD: 1 (orig: 1,2,3,4), absence: 0
clev_atrain_y = clev_atrain_y > 0
hungarian_atest_y = hungarian_atest_y > 0


correct, sens, spec, acc =[],[],[],[]


for i in range(100):
    
    ## Train a Support vector machine for cleveland training and hungarian evaluation data
    # Defining the classifier
    classifier2 = SVC(kernel="linear")
    
    # Training the classifier
    classifier2.fit(clev_atrain_x, clev_atrain_y)
    
    # Testing the classifier
    y_pred_hung = classifier2.predict(hungarian_atest_x)

    test_correct, test_sens, test_spec, test_acc = evaluation(hungarian_atest_y, y_pred_hung)
    correct.append(test_correct)
    sens.append(test_sens)
    spec.append(test_spec)
    acc.append(test_acc)
    

# evaluate model trained on cleveland dataset and tested on hungarian dataset

print("The percentage of correctly predicted targets in hungarian dataset is {}".format(test_correct))
print("The test sensitivity is {}".format(test_sens))
print("The test specificity is {}".format(test_spec))
print("The test accuracy is {}".format(test_acc))


