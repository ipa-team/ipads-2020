# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 21:09:22 2020

@author: rapha
"""
import numpy as np
import cv2
import os
from sklearn.utils import shuffle

"""
def scale(X, x_min, x_max):
    nom = (X-X.min(axis=0))*(x_max-x_min)
    denom = X.max(axis=0) - X.min(axis=0)
    denom[denom==0] = 1
    return x_min + nom/denom 
"""


def scale(X, x_min, x_max):
    return (X / 255)


def load_train_test_data(fold, magnification, resize=(140, 92, 1), path=".", bw=True, norm=True):
    """


    Parameters
    ----------
    fold : int
        1-5
        The data fold that can be loaded from the files, all loads all folds together.
    magnification : int
        40, 100, 200, 400
        Images of which magnification should be loaded.
    resize : tuple (x, y, z), optional
        Resizes loaded images to (x, y, z), if bw: z = 1.
    path: string/path
        path where foldX folders are

    Returns
    -------
    X_train : numpy array with
        DESCRIPTION.
    y_train : TYPE
        DESCRIPTION.
    X_test : TYPE
        DESCRIPTION.
    y_test : TYPE
        DESCRIPTION.

    """
    resize_n = resize[0:2][::-1]
    testpath = path + "/fold{}/test/{}X/".format(fold, magnification)
    trainpath = path + "/fold{}/train/{}X/".format(fold, magnification)

    testfiles = os.listdir(testpath)
    test_benign = [elem for elem in testfiles if elem[0:5] == "SOB_B"]
    test_malignant = [elem for elem in testfiles if elem[0:5] == "SOB_M"]

    trainfiles = os.listdir(trainpath)
    train_benign = [elem for elem in trainfiles if elem[0:5] == "SOB_B"]
    train_malignant = [elem for elem in trainfiles if elem[0:5] == "SOB_M"]

    tebe_list = []
    for file in test_benign:
        if bw:
            img = cv2.imread(testpath + file, 0)
        else:
            img = cv2.imread(testpath + file, 1)
        if resize != None:
            img = cv2.resize(img, dsize=resize_n)
        if norm:
            img = scale(img, 0, 1)
        tebe_list.append(img)
    tebe_arr = np.array(tebe_list)

    tema_list = []
    for file in test_malignant:
        if bw:
            img = cv2.imread(testpath + file, 0)
        else:
            img = cv2.imread(testpath + file, 1)
        if resize != None:
            img = cv2.resize(img, dsize=resize_n)
        if norm:
            img = scale(img, 0, 1)
        tema_list.append(img)
    tema_arr = np.array(tema_list)

    tabe_list = []
    for file in train_benign:
        if bw:
            img = cv2.imread(trainpath + file, 0)
        else:
            img = cv2.imread(trainpath + file, 1)
        if resize != None:
            img = cv2.resize(img, dsize=resize_n)
        if norm:
            img = scale(img, 0, 1)
        tabe_list.append(img)
    tabe_arr = np.array(tabe_list)

    tama_list = []
    for file in train_malignant:
        if bw:
            img = cv2.imread(trainpath + file, 0)
        else:
            img = cv2.imread(trainpath + file, 1)
        if resize != None:
            img = cv2.resize(img, dsize=resize_n)
        if norm:
            img = scale(img, 0, 1)
        tama_list.append(img)
    tama_arr = np.array(tama_list)

    X_test = np.append(tebe_arr, tema_arr, axis=0)
    y_test = np.append(np.zeros(len(test_benign)), np.ones(len(test_malignant)))
    X_test, y_test = shuffle(X_test, y_test)

    X_train = np.append(tabe_arr, tama_arr, axis=0)
    y_train = np.append(np.zeros(len(train_benign)), np.ones(len(train_malignant)))
    X_train, y_train = shuffle(X_train, y_train)

    if bw:
        X_train = np.expand_dims(X_train, axis=3)
        X_test = np.expand_dims(X_test, axis=3)

    return X_train, y_train, X_test, y_test


def load_all_data(magnification, resize=(140, 92, 1), path=".", bw=True, norm=True):
    folds = [2, 3, 4, 5]
    X_train, y_train, X_test, y_test = load_train_test_data(1, magnification, resize, path, bw)
    for i in folds:
        X_train_new, y_train_new, X_test_new, y_test_new = load_train_test_data(i, magnification, resize, path, bw)
    X_train = np.append(X_train, X_train_new, axis=0)
    y_train = np.append(y_train, y_train_new, axis=0)
    X_test = np.append(X_test, X_test_new, axis=0)
    y_test = np.append(y_test, y_test_new, axis=0)
    return X_train, y_train, X_test, y_test

# X_train, y_train, X_test, y_test = load_all_data(magnification = 40)
