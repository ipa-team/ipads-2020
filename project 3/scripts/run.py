# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 21:55:03 2020

@author: rapha
"""


from network import classification, short_class, dense_net, short_class_bin

from data_loading import load_train_test_data, load_all_data
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

fold = 1
#choose between 1 and 5
magnification = 40
#choose between 40,100, 200 and 400
resize = (140,92,1)
#select size to resize images to
path = "."
#select path where data is stored (fold folders)
bw = True
#select if images should be loaded in black and white
norm = True
#select if images should be normalized
lr = 0.000001
#select learning rate for model
classifier = classification(img_size = resize)
#select network to be trained
epochs = 100
#select for how many epochs the model should be trained

print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))


X_train, y_train, X_test, y_test = load_train_test_data( fold, magnification, resize, path, bw, norm)

"""y_train0 = np.expand_dims( y_train.astype(int), axis = 1)
y_train1 = np.expand_dims( (y_train == 0).astype(int), axis = 1)
y_train = np.append(y_train0, y_train1, axis = 1)

y_test0 = np.expand_dims( y_test.astype(int), axis = 1)
y_test1 = np.expand_dims( (y_test == 0).astype(int), axis = 1)
y_test = np.append(y_test0, y_test1, axis = 1)
"""

adam = tf.keras.optimizers.Adam(learning_rate=lr)
#

sel_metrics =  [tf.keras.metrics.BinaryAccuracy(), 
                tf.keras.metrics.Precision(), 
                tf.keras.metrics.Recall(), 
                tf.keras.metrics.AUC()]

classifier.compile(loss='binary_crossentropy', 
                   optimizer = adam, 
                   metrics = sel_metrics)

log = classifier.fit(X_train, y_train, epochs = epochs, 
                        batch_size = 100, 
                        validation_data=(X_test, y_test),
                        shuffle = True)

testpred = classifier.predict(X_test[0:50])
trainpred = classifier.predict(X_train[0:50])



loss= np.array(log.history["loss"])
val_loss = np.array(log.history["val_loss"])

accuracy = np.array(log.history["binary_accuracy"])
val_accuracy = np.array(log.history["val_binary_accuracy"])

precision = np.array(log.history["precision"])
val_precision = np.array(log.history["val_precision"])

recall = np.array(log.history["recall"])
val_recall = np.array(log.history["val_recall"])

auc = np.array(log.history["auc"])
val_auc = np.array(log.history["val_auc"])




fig, axs = plt.subplots(1,5, figsize = (20,4))
axs[0].plot((val_loss), label = "validation_loss, min = {}".format(round(min(val_loss), 2)))
axs[0].plot((loss), label = "train_loss, min = {}".format(round(min(loss), 2)))
axs[0].legend()
axs[0].set_title("loss_comparison_bce")
axs[0].set_xlabel("training_epochs")
#axs[0].set_ylim(top = 2)
axs[0].set_ylabel("loss")

axs[1].plot((val_accuracy), label = "validation_accuracy, max = {}".format(round(max(val_accuracy), 2)))
axs[1].plot((accuracy), label = "training_accuracy, max = {}".format(round(max(accuracy), 2)))
axs[1].legend()
axs[1].set_title("accuracy")
axs[1].set_xlabel("training_epochs")
axs[1].set_ylabel("accuracy")

axs[2].plot((val_precision), label = "validation_precision, max = {}".format(round(max(val_precision), 2)))
axs[2].plot((precision), label = "training_precision, max = {}".format(round(max(precision), 2)))
axs[2].legend()
axs[2].set_title("precision")
axs[2].set_xlabel("training_epochs")
axs[2].set_ylabel("precision")

axs[3].plot((val_recall), label = "validation_recall, max = {}".format(round(max(val_recall), 2)))
axs[3].plot((recall), label = "training_recall, max = {}".format(round(max(recall), 2)))
axs[3].legend()
axs[3].set_title("recall")
axs[3].set_xlabel("training_epochs")
axs[3].set_ylabel("recall")

axs[4].plot((val_auc), label = "validation_auc, max = {}".format(round(max(val_auc), 2)))
axs[4].plot((auc), label = "training_auc, max = {}".format(round(max(auc), 2)))
axs[4].legend()
axs[4].set_title("auc")
axs[4].set_xlabel("training_epochs")
axs[4].set_ylabel("auc")



