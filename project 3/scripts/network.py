# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 22:01:12 2020

@author: rapha
"""

from tensorflow.python.keras.engine.sequential import Sequential
from tensorflow.python.keras.layers import Dense, BatchNormalization, LeakyReLU, Reshape, Conv2DTranspose, Activation, UpSampling2D, LSTM, Dropout
from tensorflow.python.keras import Input, Model
from tensorflow.python.keras.layers import Conv2D, MaxPool2D, Flatten, Conv1D, MaxPooling1D


def classification(img_size):
    return Sequential([
        Input(shape  = img_size),
		Conv2D(filters = 128, kernel_size = (6,6), strides = (2,2)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),
        
        Conv2D(filters = 128, kernel_size = (5,5), strides = (2,2)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),

        Conv2D(filters = 128, kernel_size = (4,4), strides = (2,2)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),

        Conv2D(filters = 128, kernel_size = (3,3), strides = (2,2)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),

        Flatten(),
        Dense(128),
        LeakyReLU(),
        Dense(1),
        
		Activation("sigmoid")
		])

def short_class(img_size):
    return Sequential([
        Input(shape  = img_size),
		Conv2D(filters = 128, kernel_size = (6,6), strides = (4,4)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),
        Conv2D(filters = 128, kernel_size = (3,3), strides = (4,4)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),
        Flatten(),
        Dense(128),
        LeakyReLU(),
        Dense(1),
        
		Activation("sigmoid")
		])

def short_class_bin(img_size):
    return Sequential([
        Input(shape  = img_size),
		Conv2D(filters = 128, kernel_size = (3,3), strides = (4,4)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),
        Conv2D(filters = 128, kernel_size = (3,3), strides = (4,4)),
        LeakyReLU(),
        BatchNormalization(),
        Dropout(0.3),
        Flatten(),
        Dense(128),
        LeakyReLU(),
        Dense(2),
        
		Activation("sigmoid")
		])

def dense_net(img_size):
    return Sequential([
        Input(shape  = img_size),
        Flatten(),
		Dense(100 ),
        LeakyReLU(),
        Dense(20 ), 
        LeakyReLU(),
        Dense(1 ),
		Activation("sigmoid")
		])

"""

from tensorflow.keras.utils import plot_model

model1 = classification((140,92,1))
model2= short_class((140,92,1))

plot_model(model1, 
           to_file = 'long_classification.png', 
           show_shapes = True, 
           show_layer_names = False, 
           expand_nested = True)

plot_model(model2, 
           to_file = 'short_classification.png', 
           show_shapes = True, 
           show_layer_names = False, 
           expand_nested = True)
"""