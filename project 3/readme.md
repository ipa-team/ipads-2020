# Readme

The final skript is called Project 3.ipynb. 
The final report is called Project 3.pdf

## Manual local Notebook:
It is assumed, that the folders "fold1" to "fold5", as provided by breakHis using the script mkfold, are present in the same directory as Project 3.ipynb 

## Manual Colab:
You can also use the google colab notebook:

https://colab.research.google.com/drive/1V0TbzlwpHlfIQyzt4PSRp86oyKmL3dX5#scrollTo=heq7g-fU2fQd

It is assumed, that the breakHis_v1.tar.gz & mkfold.tar.gz are located within the google drive folder

## Contributions
Commits do not represent contribution since work was also done and contributed outside this repository.